#include <string>
#include <random>

using std::string;

string randDNA(int s, std::string b, int n)
{
	int i;
	int min = 0;
	int max = b.length() -1;
	std::string ATCG = "";
	
	std::mt19937 eng(s);
	std::uniform_int_distribution <> uniform(min,max);
	
	for (i = 0; i < n; i++)
		ATCG += b[uniform(eng)];
		
return ATCG;
}
